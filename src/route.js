import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './header.css'
import Sticky from 'react-sticky-el'
import hello from './hello.js'
import list from './List.js'

function Home() {
    return (
        <Router>
            <div>
                <Sticky stickyClassName={'Stick'}>
                    <h1>
                            <ul>
                                <li>
                                    <Link to="/">Home</Link>
                                </li>
                                <li>
                                    <Link to="/list">List</Link>
                                </li>
                            </ul>
                    </h1>
                <hr />
                </Sticky>
                <Route exact path="/" component={hello} />
                <Route path="/list" component={list} />
            </div>
        </Router>
    );
}

export default Home