import React from 'react'
let randomSentence = require('random-sentence');

class List extends React.Component {

    createList = () => {
        let table = []
        for (let i = 0; i < 100000; i++) {
            table.push(randomSentence({min: 5,max: 9}));
        }
        return table
    }

    render() {
        return(
            <p>
                {this.createList()}
            </p>
        )
    }
}

export default List